# Introduction
The Contacts Subscriptions module adds subscription/membership functionality to
a site.

This functionality includes:
* Users can purchase and manage subscriptions with renewal dates.
* A workflow with statuses to manage subscription changes and renewal.
* Drupal Commerce and Stripe integration for paid subscriptions.

# Requirements
This module requires the following modules:

* Contacts (project/contacts)
* Commerce Order (project/commerce)
* Commerce Price (project/commerce)
* Commerce Product (project/commerce)
* Commerce Stripe (project/commerce_stripe)
* Cron Queue Invoker (project/cron_queue_invoker)

# Installation
Use [Composer](https://getcomposer.org/) to include Contacts Subscriptions with
all dependencies.

See Drupal's [module install documentation](https://www.drupal.org/docs/extending-drupal/installing-modules#s-add-a-module-with-composer) for more details.

## Configuration
Upon installation, the following steps are recommended for basic usage:

* Create a Subscription Type through config or the UI at
  /admin/commerce/config/subscription-types.
* Update the default form display for your new Subscription Type.*
* Create a Commerce Product of type `Subscription` for membership.
* Ensure at least one Stripe payment gateway exists.**

*This should be automated in the future.

**Other gateways will hopefully be supported in the future.

# Usage
Once installed:
* Users with access to their user dashboard can purchase and manage
subscriptions from /user/{id}/subscription.
* Users with the `administer subscriptions types` permission can manage
subscriptions from /admin/commerce/subscriptions

# Maintainers
Current maintainers:
* Andrew Belcher (andrewbelcher) - /u/andrewbelcher
* Yan Loetzer (yanniboi) - /u/yanniboi
* Mark Jones (justanothermark) - /u/justanothermark

This project has been sponsored by:
* FreelyGive

  We're FreelyGive, Drupal specialists developing Full Stack Customer
  Relationship Management (CRM) solutions your users will love.

  Visit https://freelygive.io/ for more information.
