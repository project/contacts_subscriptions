<?php

namespace Drupal\contacts_subscriptions\Plugin\views\filter;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\views\Plugin\views\filter\ManyToOne;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter for subscription product.
 *
 * @ViewsFilter("contacts_subscription_products")
 */
class SubscriptionProducts extends ManyToOne {

  /**
   * The product entity storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected ContentEntityStorageInterface $productStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $plugin->productStorage = $container->get('entity_type.manager')->getStorage('commerce_product');
    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    if ($this->valueOptions === NULL) {
      $this->valueOptions = array_map(
        fn (ProductInterface $product) => $product->label(),
        $this->productStorage->loadByProperties(['type' => 'subscription']),
      );
    }
    return $this->valueOptions;
  }

}
