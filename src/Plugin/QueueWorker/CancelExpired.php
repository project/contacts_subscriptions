<?php

namespace Drupal\contacts_subscriptions\Plugin\QueueWorker;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Queue worker to expire subscriptions.
 *
 * @QueueWorker(
 *   id = "contacts_subscription_cancel_expired",
 *   title = @Translation("Cancel any expired subscriptions, taking account of the grace period."),
 *   cron = {"time" = 30}
 * )
 */
class CancelExpired extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The user entity storage handler.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected ContentEntityStorageInterface $subscriptionStorage;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $plugin = new static($configuration, $plugin_id, $plugin_definition);
    $plugin->subscriptionStorage = $container->get('entity_type.manager')->getStorage('contacts_subscription');

    return $plugin;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if ($subscription = $this->subscriptionStorage->load($data)) {

      // If the subscription is not active there is nothing to do.
      if ($subscription->isActive()) {
        $date = new DrupalDateTime();
        $date->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);

        if ($subscription->getExpiryDate() < $date) {

          /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface $status */
          $status = $subscription->get('status')->first();
          $status->applyTransitionById('cancel');
          $subscription->setNewRevision(TRUE);
          $subscription->setRevisionCreationTime(time());
          $subscription->setRevisionLogMessage('Cancelled after grace period.');
          $subscription->save();
        }
      }
    }
  }

}
