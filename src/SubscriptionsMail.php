<?php

namespace Drupal\contacts_subscriptions;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;

/**
 * Hook_mail implementation for Contacts Subscription.
 */
class SubscriptionsMail {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected Token $token;

  /**
   * The subscription helper service.
   *
   * @var \Drupal\contacts_subscriptions\SubscriptionsHelper
   */
  protected SubscriptionsHelper $subscriptionsHelper;

  /**
   * Constructs a SubscriptionsMail object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\contacts_subscriptions\SubscriptionsHelper $subscriptions_helper
   *   The subscriptions helper service.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    DateFormatterInterface $date_formatter,
    RendererInterface $renderer,
    Token $token,
    SubscriptionsHelper $subscriptions_helper
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->renderer = $renderer;
    $this->token = $token;
    $this->subscriptionsHelper = $subscriptions_helper;
  }

  /**
   * Method description.
   */
  public function paymentSuccess(array &$message, array $params) {
    $message['subject'] = $this->token->replace('Thank you for your membership payment at [site:name]');

    $message['headers']['Content-Type'] = 'text/html; charset=UTF-8;';
    $message['headers']['Content-Transfer-Encoding'] = '8Bit';

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $params['order'];
    $subscription = $this->subscriptionsHelper->getSubscriptionFromOrder($order);

    $body = [
      '#theme' => 'contacts_subscription_mail_payment_success',
      '#order_entity' => $order,
      '#subscription_entity' => $subscription,
      '#subscription' => [],
    ];

    $body['#subscription']['type'] = $subscription->getProduct(FALSE, FALSE)->label();

    if ($date = $subscription->getRenewalDate(FALSE)) {
      $body['#subscription']['renewal_date'] = $this->dateFormatter->format(
        $date->getTimestamp(),
        'medium_date'
      );
    }

    if ($billing_profile = $order->getBillingProfile()) {
      $body['#billing_information'] = $this->entityTypeManager
        ->getViewBuilder('profile')
        ->view($billing_profile);
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $order->get('payment_method')->entity;
    if ($payment_method) {
      $body['#payment_method'] = [
        '#markup' => $payment_method->label(),
      ];
    }

    $message['body'] = [$this->renderer->render($body)];
  }

  /**
   * Method description.
   */
  public function paymentFailure(array &$message, array $params) {
    $message['subject'] = $this->token->replace('Your payment has failed');

    $message['headers']['Content-Type'] = 'text/html; charset=UTF-8;';
    $message['headers']['Content-Transfer-Encoding'] = '8Bit';

    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $params['order'];
    $subscription = $this->subscriptionsHelper->getSubscriptionFromOrder($order);

    $body = [
      '#theme' => 'contacts_subscription_mail_payment_failure',
      '#order_entity' => $order,
      '#subscription_entity' => $subscription,
      '#subscription' => [],
    ];

    $body['#subscription']['type'] = $subscription->getProduct(FALSE, FALSE)->label();
    $body['#subscription']['renewal_date'] = $this->dateFormatter->format(
      $order->getCreatedTime(),
      'medium_date'
    );
    $body['#subscription']['expiration_date'] = $this->dateFormatter->format(
      DrupalDateTime::createFromTimestamp($order->getCreatedTime())
        // @todo Pull this from configuration.
        ->add(new \DateInterval('P1M'))->getTimestamp(),
      'medium_date'
    );

    $payment_url = Url::fromRoute(
      'contacts_subscriptions.subscription_payment',
      ['user' => $subscription->getOwnerId()],
    );
    $body['#subscription']['renewal_link'] = Url::fromRoute(
      'user.login',
      [],
      ['query' => ['destination' => $payment_url->toString()]],
    )->setAbsolute(TRUE)->toString();

    if ($billing_profile = $order->getBillingProfile()) {
      $body['#billing_information'] = $this->entityTypeManager
        ->getViewBuilder('profile')
        ->view($billing_profile);
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
    $payment_method = $order->get('payment_method')->entity;
    if ($payment_method) {
      $body['#payment_method'] = [
        '#markup' => $payment_method->label(),
      ];
    }

    $message['body'] = [$this->renderer->render($body)];
  }

}
