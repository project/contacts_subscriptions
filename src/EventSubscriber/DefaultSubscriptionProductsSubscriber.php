<?php

namespace Drupal\contacts_subscriptions\EventSubscriber;

use Drupal\contacts_subscriptions\Event\SubscriptionProductVariationsEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The default subscription product subscriber.
 */
class DefaultSubscriptionProductsSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Add all subscription products as default.
   *
   * @param \Drupal\contacts_subscriptions\Event\SubscriptionProductVariationsEvent $event
   *   The job product variation event.
   */
  public function getAllSubscriptions(SubscriptionProductVariationsEvent $event) {
    $event->allowProducts(
      $this->entityTypeManager
        ->getStorage('commerce_product')
        ->getQuery()
        ->condition('type', 'subscription')
        ->condition('status', TRUE)
        ->execute()
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // Get the default, running last.
      SubscriptionProductVariationsEvent::NAME => ['getAllSubscriptions', -999],
    ];
  }

}
