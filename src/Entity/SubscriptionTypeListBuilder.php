<?php

namespace Drupal\contacts_subscriptions\Entity;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Defines a class to build a listing of subscriptions type entities.
 *
 * @see \Drupal\contacts_subscriptions\Entity\SubscriptionType
 */
class SubscriptionTypeListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['title'] = $this->t('Label');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['title'] = [
      'data' => $entity->label(),
      'class' => ['menu-label'],
    ];

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    $build['table']['#empty'] = $this->t(
      'No membership types available. <a href=":link">Add membership type</a>.',
      [':link' => Url::fromRoute('entity.contacts_subscription_type.add_form')->toString()]
    );

    return $build;
  }

}
