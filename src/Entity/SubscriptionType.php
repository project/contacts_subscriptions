<?php

namespace Drupal\contacts_subscriptions\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Defines the Subscriptions type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "contacts_subscription_type",
 *   label = @Translation("Subscription type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\contacts_subscriptions\Form\SubscriptionTypeForm",
 *       "edit" = "Drupal\contacts_subscriptions\Form\SubscriptionTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\contacts_subscriptions\Entity\SubscriptionTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "local_task_provider" = {
 *       "default" = "Drupal\entity\Menu\DefaultEntityLocalTaskProvider",
 *     }
 *   },
 *   admin_permission = "administer subscriptions types",
 *   bundle_of = "contacts_subscription",
 *   config_prefix = "contacts_subscription_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "grace_period" = "grace_period",
 *     "automatic_renewals" = "automatic_renewals",
 *     "automatic_cancellations" = "automatic_cancellations",
 *     "automatic_payments" = "automatic_payments",
 *     "renew_before_expiry" = "renew_before_expiry",
 *     "days_before_expiry" = "days_before_expiry",
 *     "who_can_purchase" = "who_can_purchase"
 *   },
 *   links = {
 *     "add-form" = "/admin/commerce/config/subscription-types/add",
 *     "edit-form" = "/admin/commerce/config/subscription-types/manage/{contacts_subscription_type}",
 *     "delete-form" = "/admin/commerce/config/subscription-types/manage/{contacts_subscription_type}/delete",
 *     "collection" = "/admin/commerce/config/subscription-types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *     "grace_period",
 *     "automatic_renewals",
 *     "automatic_cancellations",
 *     "automatic_payments",
 *     "renew_before_expiry",
 *     "days_before_expiry",
 *     "who_can_purchase",
 *   }
 * )
 */
class SubscriptionType extends ConfigEntityBundleBase {

  /**
   * The machine name of this subscriptions type.
   *
   * @var string
   */
  protected string $id;

  /**
   * The human-readable name of the subscriptions type.
   *
   * @var string
   */
  protected string $label;

  /**
   * The grace period to give before cancellation, in days.
   *
   * @var int
   */
  protected int $grace_period = 0;

  /**
   * Whether to automatically renew memberships, where possible.
   *
   * @var int
   */
  protected int $automatic_renewals = 0;

  /**
   * Whether to automatically expire memberships.
   *
   * @var int
   */
  protected int $automatic_cancellations = 0;

  /**
   * Whether to attempt to take payment on automatic renewal.
   *
   * @var int
   */
  protected int $automatic_payments = 0;

  /**
   * Whether users can renew while the subscription is still active.
   *
   * @var int
   */
  protected int $renew_before_expiry = 0;

  /**
   * Whether users can renew while the subscription is still active.
   *
   * @var string|null
   */
  protected ?string $days_before_expiry = NULL;

  /**
   * Which contacts type can purchase this subscription?
   *
   * @var string
   *   The types: defaults to crm_indiv.
   */
  protected string $who_can_purchase = 'crm_indiv';

  /**
   * Helper to get grace period.
   *
   * @return int
   *   The grace period.
   */
  public function getGracePeriod(): int {
    return $this->grace_period;
  }

  /**
   * Helper to get the Automatic Renewals setting.
   *
   * @return int
   *   The Automatic Renewals value.
   */
  public function getAutomaticRenewals(): int {
    return $this->automatic_renewals;
  }

  /**
   * Helper to get the Automatic Cancellations setting.
   *
   * @return int
   *   The automatic cancellations value.
   */
  public function getAutomaticCancellations(): int {
    return $this->automatic_cancellations;
  }

  /**
   * Helper to get the Automatic Payments setting.
   *
   * @return int
   *   The automatic payments value.
   */
  public function getAutomaticPayments(): int {
    return $this->automatic_payments;
  }

  /**
   * Helper to get the Renew Before Expiry setting.
   *
   * @return int
   *   The renew before expiry value.
   */
  public function getRenewBeforeExpiry(): int {
    return $this->renew_before_expiry;
  }

  /**
   * Helper to get the Days Before Expiry setting.
   *
   * @return int|null
   *   The days before expiry value.
   */
  public function getDaysBeforeExpiry(): ?int {
    if ($this->renew_before_expiry && $this->days_before_expiry) {
      return (int) $this->days_before_expiry;
    }
    return NULL;
  }

  /**
   * Helper to set the Renew Before Expiry setting.
   *
   * @param int $int
   *   The value to set.
   */
  public function setRenewBeforeExpiry(int $int) {
    $this->renew_before_expiry = $int;
  }

  /**
   * Helper to set the Days Before Expiry setting.
   *
   * @param int|null $value
   *   The value to set.
   */
  public function setDaysBeforeExpiry(?int $value) {
    if ($this->renew_before_expiry) {
      $this->days_before_expiry = $value;
    }
  }

  /**
   * Returns the currently set Who Can Purchase role.
   *
   * @return string
   *   The role machine name.
   */
  public function getWhoCanPurchase(): string {
    return $this->who_can_purchase;
  }

  /**
   * Set the grace period setting.
   *
   * @param int $period
   *   The number of days.
   */
  public function setGracePeriod(int $period) {
    $this->grace_period = $period;
  }

  /**
   * Set automatic renewals.
   *
   * @param int $renewals
   *   The value to set.
   */
  public function setAutomaticRenewals(int $renewals) {
    $this->automatic_renewals = $renewals;
  }

  /**
   * Set automatic cancellations.
   *
   * @param int $cancellations
   *   The value to set.
   */
  public function setAutomaticCancellations(int $cancellations) {
    $this->automatic_cancellations = $cancellations;
  }

  /**
   * Set automatic payments.
   *
   * @param int $payments
   *   The value to set.
   */
  public function setAutomaticPayments(int $payments) {
    $this->automatic_payments = $payments;
  }

  /**
   * Set the Who Can Purchase role.
   *
   * @param string $role
   *   The role machine name.
   */
  public function setWhoCanPurchase(string $role) {
    $this->who_can_purchase = $role;
  }

  /**
   * Returns the ids of any subscriptions of this type ready to renew.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $renewal
   *   A DrupalDateTime to exclude renewal dates after.
   *
   * @return mixed
   *   An array of ids, or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getRenewable(?DrupalDateTime $renewal = NULL) {
    return $this->getIdsByStatus(['active', 'needs_payment'], $renewal);
  }

  /**
   * Returns the ids of any subscriptions of this type ready to cancel.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $renewal
   *   A DrupalDateTime to exclude renewal dates after.
   *
   * @return mixed
   *   An array of ids, or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getCancellable(?DrupalDateTime $renewal = NULL) {
    return $this->getIdsByStatus([
      'expired_payment_pending',
      'cancelled_pending',
      'needs_payment',
    ]);
  }

  /**
   * Helper to query the database.
   *
   * @param array $status
   *   Valid statuses for the subscriptions to be in.
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $renewal
   *   A DrupalDateTime to exclude renewal dates after.
   *
   * @return mixed
   *   An array of ids or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  private function getIdsByStatus(array $status = [], ?DrupalDateTime $renewal = NULL) {
    $query = $this->entityTypeManager()->getStorage('contacts_subscription')->getQuery();
    $query->accessCheck(FALSE);
    $query->condition('bundle', $this->id());

    if ($renewal) {
      $query->condition('renewal', $renewal, '<=');
    }

    if ($status) {
      $query->condition('status', $status, 'IN');
    }
    $query->sort('renewal');

    return $query->execute();
  }

}
