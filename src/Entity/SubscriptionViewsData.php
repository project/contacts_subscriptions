<?php

namespace Drupal\contacts_subscriptions\Entity;

use Drupal\views\EntityViewsData;

/**
 * Views data for the subscription entity.
 */
class SubscriptionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['contacts_subscription']['table']['join']['contacts_subscription_revision'] = [
      'left_field' => 'id',
      'field' => 'id',
      'type' => 'INNER',
    ];

    $data['contacts_subscription']['status']['filter']['id'] = 'state_machine_state';

    $data['contacts_subscription']['uid']['help'] = $this->t('The user the subscription belongs to');
    $data['contacts_subscription']['uid']['filter']['id'] = 'user_name';
    $data['contacts_subscription']['uid']['relationship']['title'] = $this->t('Owner');
    $data['contacts_subscription']['uid']['relationship']['help'] = $this->t('Relate subscription to the user to whom it belongs.');
    $data['contacts_subscription']['uid']['relationship']['label'] = $this->t('Owner');

    $data['contacts_subscription']['product']['filter']['id'] = 'contacts_subscription_products';

    $data['users']['contacts_subscription']['help'] = $this->t("The user's membership");
    $data['users']['contacts_subscription']['relationship']['title'] = $this->t('Membership');
    $data['users']['contacts_subscription']['relationship']['help'] = $this->t("The user's membership");
    $data['users']['contacts_subscription']['relationship']['label'] = $this->t('Membership');
    $data['users']['contacts_subscription']['relationship']['id'] = 'standard';
    $data['users']['contacts_subscription']['relationship']['base'] = 'contacts_subscription';
    $data['users']['contacts_subscription']['relationship']['base field'] = 'uid';
    $data['users']['contacts_subscription']['relationship']['field'] = 'uid';

    return $data;
  }

}
