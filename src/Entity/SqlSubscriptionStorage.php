<?php

namespace Drupal\contacts_subscriptions\Entity;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\user\UserInterface;

/**
 * SQL storage for subscriptions.
 */
class SqlSubscriptionStorage extends SqlContentEntityStorage {

  /**
   * Load a subscription for the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   * @param string|null $type
   *   The subscription type.
   *
   * @return \Drupal\contacts_subscriptions\Entity\SubscriptionInterface[]
   *   The subscriptions.
   */
  public function loadByUser(UserInterface $user, string $type = NULL): ?array {
    if ($user->isAnonymous()) {
      return [];
    }

    $values = [
      'uid' => $user->id(),
    ];

    if ($type) {
      $values['bundle'] = $type;
    }

    $subscriptions = $this->loadByProperties($values);
    usort($subscriptions, [Subscription::class, 'sort']);

    return $subscriptions ?: [];
  }

}
