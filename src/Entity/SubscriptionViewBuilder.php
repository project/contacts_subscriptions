<?php

namespace Drupal\contacts_subscriptions\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for a subscriptions entity type.
 */
class SubscriptionViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    // The subscriptions has no entity template itself.
    unset($build['#theme']);
    return $build;
  }

}
