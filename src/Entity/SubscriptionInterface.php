<?php

namespace Drupal\contacts_subscriptions\Entity;

use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining a subscription entity type.
 */
interface SubscriptionInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface, RevisionLogInterface {

  /**
   * Get the current status id.
   *
   * @return string
   *   The current status id.
   */
  public function getStatusId(): string;

  /**
   * Get the label for the current status.
   *
   * @return string|null
   *   The label for the current status, if any.
   */
  public function getStatusLabel(): ?string;

  /**
   * Get the status item.
   *
   * @return \Drupal\state_machine\Plugin\Field\FieldType\StateItemInterface
   *   The status item.
   */
  public function getStatusItem(): StateItemInterface;

  /**
   * Check if the subscription is active.
   *
   * @return bool
   *   Whether the subscription is active.
   */
  public function isActive(): bool;

  /**
   * Get the expiry date.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   The expiry date or NULL if the subscription is not active.
   */
  public function getExpiryDate(): ?DrupalDateTime;

  /**
   * Check if the subscription is ready for automatic renewal.
   *
   * @return bool
   *   Whether the subscription will renew.
   */
  public function willAutoRenew(): bool;

  /**
   * Check if the subscription is ready for a user to renew.
   *
   * @return bool
   *   Whether the subscription will renew.
   */
  public function willRenew(): bool;

  /**
   * Get the renewal date.
   *
   * @param bool $check_status
   *   Whether to check the status before returning.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime|null
   *   The renewal date or NULL if there is none or the subscription will not
   *   renew.
   */
  public function getRenewalDate(bool $check_status = TRUE): ?DrupalDateTime;

  /**
   * Whether we need to collect or re-validate payment details.
   *
   * @return bool
   *   Whether we need to collect or re-validate payment details.
   */
  public function needsPaymentDetails(): bool;

  /**
   * Whether the subscription has been cancelled but is currently active.
   *
   * @return bool
   *   Whether the subscription has been cancelled but is currently active.
   */
  public function isCancelPending(): bool;

  /**
   * The product subscribed to.
   *
   * @param bool $check_status
   *   Whether to check the status before returning.
   * @param bool $renewal_product
   *   Whether to use the renewal product field instead of the product field.
   *
   * @return \Drupal\commerce_product\Entity\ProductInterface|null
   *   The product, or NULL if there is no active subscription.
   */
  public function getProduct(bool $check_status = TRUE, bool $renewal_product = FALSE): ?ProductInterface;

  /**
   * The product ID subscribed to.
   *
   * @param bool $check_status
   *   Whether to check the status before returning.
   * @param bool $renewal_product
   *   Whether to use the renewal product field instead of the product field.
   *
   * @return int|null
   *   The product ID, or NULL if there is no active subscription.
   */
  public function getProductId(bool $check_status = TRUE, bool $renewal_product = FALSE): ?int;

  /**
   * Generate the CSRF value for a given variation.
   *
   * As available variation IDs may be specific to the current state and product
   * of the subscription, we use those in the token. The UID ties it to the
   * organisation we're working with. We use the renewal date to both time limit
   * and invalidate when automatic changes happen. The target variation is
   * included to ensure the token is valid for that specific variation.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $variation
   *   The variation being offered.
   *
   * @return string
   *   The CSRF value.
   */
  public function getCsrfValue(ProductVariationInterface $variation): string;

  /**
   * Get the discounted renewal price, if any.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The discounted price, or NULL if there is no valid discount.
   */
  public function getOverriddenPrice(): ?Price;

  /**
   * Check whether a discount has expired.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $date
   *   Optionally a date for the check, e.g. the next renewal.
   *
   * @return bool|null
   *   Whether the discount has expired, or NULL if there is no discount.
   */
  public function hasOverrideExpired(?DrupalDateTime $date = NULL): ?bool;

  /**
   * Checks whether the product can be changed.
   *
   * Can't change product if a renewal product is set, which prevents
   * the product being changed multiple times in a billing cycle.
   *
   * @return bool
   *   Whether the product can be changed.
   */
  public function canChangeProduct() : bool;

  /**
   * Whether the subscription is going to change at next renewal.
   *
   * @return bool
   *   TRUE if the subscription will change at the start of the next billing
   *   cycle.
   */
  public function isChanging() : bool;

}
