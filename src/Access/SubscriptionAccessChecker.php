<?php

namespace Drupal\contacts_subscriptions\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\Routing\Route;

/**
 * Checks if passed parameter matches the route configuration.
 *
 * @DCG
 * To make use of this access checker add '_subscription_access: Some value'
 * entry to route definition under requirements section.
 */
class SubscriptionAccessChecker implements AccessInterface {

  /**
   * An Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constrct the Access Checker.
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   The logged in user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The Entity Type Manager.
   */
  public function __construct(AccountProxyInterface $user, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Check whether the user has access to this user's membership.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route being checked.
   * @param int $user
   *   The uid variable passed to the route.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function access(Route $route, int $user, AccountInterface $account) {
    $return = AccessResult::forbidden()->addCacheContexts(['url']);

    if ($user = $this->entityTypeManager->getStorage('user')->load($user)) {

      if ($user->access('dashboard', $account)) {
        $return = AccessResult::allowed()->addCacheContexts(['url']);
      }

      // If access is denied but the user has the permission to manage their
      // organisation's membership we should check if they are trying to access
      // the form for that organisation.
      if (!$user->access('dashboard', $account) && $account->hasPermission('manage my organisation membership')) {
        $account = $this->entityTypeManager->getStorage('user')->load($account->id());

        if ($account->hasField('organisations')) {
          foreach ($account->organisations as $item) {
            if ($group = $item->membership->getGroup()) {
              if ($group->contacts_org->target_id == $user->id()) {
                $return = AccessResult::allowed()->addCacheContexts(['url']);
              }
            }
          }
        }
      }
    }

    return $return;
  }

}
