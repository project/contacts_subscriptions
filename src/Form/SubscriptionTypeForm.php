<?php

namespace Drupal\contacts_subscriptions\Form;

use Drupal\contacts_subscriptions\Entity\SubscriptionType;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for subscriptions type forms.
 */
class SubscriptionTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    /** @var \Drupal\contacts_subscriptions\Entity\SubscriptionType $entity_type */
    $entity_type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add membership type');
    }
    else {
      $form['#title'] = $this->t(
        'Edit %label membership type',
        ['%label' => $entity_type->label()]
      );
    }

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $entity_type->label(),
      '#description' => $this->t('The human-readable name of this membership type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $entity_type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => [SubscriptionType::class, 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('A unique machine-readable name for this membership type. It must only contain lowercase letters, numbers, and underscores.'),
    ];

    $form['grace_period'] = [
      '#type' => 'number',
      '#title' => $this->t('Grace Period'),
      '#default_value' => $entity_type->getGracePeriod(),
      '#description' => $this->t('A number of days to wait between failure to renew and fully cancelling membership.'),
    ];
    $form['automatic_renewals'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatic Renewals'),
      '#default_value' => $entity_type->getAutomaticRenewals(),
      '#description' => $this->t('Automatically renew memberships when the expiry date passes, if the user has a valid payment method on file.'),
    ];
    $form['automatic_payments'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatic Payments'),
      '#default_value' => $entity_type->getAutomaticPayments(),
      '#description' => $this->t('Attempt to take payment using stored credentials when this membership is automatically renewed.'),
      '#states' => [
        'visible' => [
          ':input[name="automatic_renewals"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['renew_before_expiry'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Renew Before Expiry'),
      '#default_value' => $entity_type->getRenewBeforeExpiry(),
      '#description' => $this->t('Allow users to renew their memberships before they have expired.'),
    ];
    $form['days_before_expiry'] = [
      '#type' => 'number',
      '#title' => $this->t('Days Before Renewal'),
      '#default_value' => $entity_type->getDaysBeforeExpiry(),
      '#description' => $this->t('Enter the number of days before expiry the user can renew. 0 will allow renewal on day of expiry: no value will mean the user can always renew, even if they have just completed a renewal.'),
      '#states' => [
        'visible' => [
          ':input[name="renew_before_expiry"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['automatic_cancellations'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Automatic Cancellations'),
      '#default_value' => $entity_type->getAutomaticCancellations(),
      '#description' => $this->t('Automatically cancel memberships when the expiry date passes or renewal has failed (if selected above), after the grace period has passed.'),
    ];
    $form['who_can_purchase'] = [
      '#type' => 'select',
      '#title' => $this->t('Who can purchase?'),
      '#default_value' => $entity_type->getWhoCanPurchase(),
      '#options' => [
        'both' => $this->t('All Contacts'),
        'crm_indiv' => $this->t('Individuals'),
        'crm_org' => $this->t('Organisations'),
      ],
      '#description' => $this->t('Restrict this subscription to only be allowed to be purchased by contacts with specific roles. Select Both to allow all contacts to purchase it.'),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save');
    $actions['delete']['#value'] = $this->t('Delete');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    /** @var \Drupal\contacts_subscriptions\Entity\SubscriptionType $entity_type */
    $entity_type = $this->entity;

    $entity_type->set('id', trim($entity_type->id()));
    $entity_type->set('label', trim($entity_type->label()));
    $entity_type->setGracePeriod($form_state->cleanValues()->getValue('grace_period'));
    $entity_type->setAutomaticRenewals($form_state->cleanValues()->getValue('automatic_renewals'));
    $entity_type->setAutomaticCancellations($form_state->cleanValues()->getValue('automatic_cancellations'));
    $entity_type->setWhoCanPurchase($form_state->cleanValues()->getValue('who_can_purchase'));
    $entity_type->setRenewBeforeExpiry($form_state->cleanValues()->getValue('renew_before_expiry'));

    if ($entity_type->getAutomaticRenewals()) {
      $entity_type->setAutomaticPayments($form_state->cleanValues()->getValue('automatic_payments'));
    }
    else {
      $entity_type->setAutomaticPayments(0);
    }
    if ($entity_type->getRenewBeforeExpiry()) {
      $value = $form_state->cleanValues()->getValue('days_before_expiry');
      if ($value !== '') {
        $entity_type->setDaysBeforeExpiry($form_state->cleanValues()->getValue('days_before_expiry'));
      }
      else {
        $entity_type->setDaysBeforeExpiry(NULL);
      }
    }

    $status = $entity_type->save();

    $t_args = ['%name' => $entity_type->label()];
    if ($status == SAVED_UPDATED) {
      $message = $this->t('The membership type %name has been updated.', $t_args);
    }
    elseif ($status == SAVED_NEW) {
      $message = $this->t('The membership type %name has been added.', $t_args);
    }
    $this->messenger()->addStatus($message);

    $form_state->setRedirectUrl($entity_type->toUrl('collection'));
  }

}
