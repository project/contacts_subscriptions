<?php

namespace Drupal\contacts_subscriptions\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;

/**
 * Form controller for the subscriptions entity edit forms.
 */
class SubscriptionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  protected function prepareEntity() {
    parent::prepareEntity();

    // Clear the default value on the subscription user.
    if ($this->entity->isNew()) {
      $this->entity->set('uid', NULL);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    // Price must be specified if end date is provided.
    if (isset($form['price_override']) && isset($form['price_override_date'])) {
      $form['price_override_date']['#states'] = [
        'visible' => [
          ':input[name="price_override[0][value]"]' => ['filled' => TRUE],
        ],
      ];
    }

    // A Renewal Date is required for all active subscriptions, so ensure the
    // field appears when a subscription is set to active.
    if (isset($form['renewal']) && isset($form['status'])) {
      if (!empty($form['renewal']['widget'][0]['has_value'])) {
        $form['renewal']['widget'][0]['has_value']['#states'] = [
          'checked' => [
            'select#edit-status' => ['value' => 'active'],
          ],
        ];
      }
    }

    // Always create a new revision.
    if (!$this->entity->isNew()) {
      $form['revision']['#default_value'] = TRUE;
      $form['revision']['#disabled'] = TRUE;

      if (isset($form['uid'])) {
        $form['uid']['widget']['#disabled'] = TRUE;
      }
    }
    else {

      // Allow the URL to select the user a subscription should be linked to for
      // new subscriptions.
      if ($uid = $this->getRequest()->query->get('uid')) {
        /** @var \Drupal\decoupled_auth\Entity\DecoupledAuthUser $account */
        if ($account = $this->entityTypeManager->getStorage('user')->load($uid)) {
          if ($account->hasRole('crm_indiv')) {
            $form['uid']['widget'][0]['target_id']['#default_value'] = $account;
          }
        }
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions = parent::actions($form, $form_state);

    // Prepare cancel link.
    $query = $this->getRequest()->query;
    $url = NULL;
    // If a destination is specified, that serves as the cancel link.
    if ($query->has('destination')) {
      $options = UrlHelper::parse($query->get('destination'));
      try {
        $url = Url::fromUserInput('/' . ltrim($options['path'], '/'), $options);
      }
      catch (\InvalidArgumentException $e) {
        // Suppress the exception and fall back to the form's cancel url.
      }
    }

    $actions['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Cancel'),
      '#attributes' => ['class' => ['button']],
      '#url' => $url ?? Url::fromRoute('entity.contacts_subscription.collection'),
      '#cache' => [
        'contexts' => [
          'url.query_args:destination',
        ],
      ],
    ];

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_subscriptions\Entity\SubscriptionInterface $subscription */
    $subscription = parent::validateForm($form, $form_state);

    if (isset($form['price_override']) && isset($form['price_override_date'])) {
      $has_price = !$subscription->get('price_override')->isEmpty();
      $has_end_date = !$subscription->get('price_override_date')->isEmpty();
      if ($has_end_date && !$has_price) {
        $form_state->setError(
          $form['price_override']['widget'],
          new TranslatableMarkup('A price override must be set if an end date is specified.')
        );
      }
    }

    // @todo Move these to constraints.
    $status = $subscription->getStatusId();
    if ($status === 'active') {
      if (!$subscription->getRenewalDate(FALSE)) {
        $form_state->setError(
          $form['renewal']['widget'],
          new TranslatableMarkup('A renewal must be set for an active membership.')
        );
      }
    }
    if ($status !== 'none') {
      if (!$subscription->getProductId(FALSE)) {
        $form_state->setError(
          $form['product']['widget'],
          new TranslatableMarkup('A product must be set for a membership and cannot be cleared.')
        );
      }
    }

    if (isset($form['price_override'])) {

      return $subscription;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New membership %label has been created.', $message_arguments));
      $this->logger('contacts_subscriptions')->notice('Created new subscription %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The membership %label has been updated.', $message_arguments));
      $this->logger('contacts_subscriptions')->notice('Updated new membership %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.contacts_subscription.canonical', ['contacts_subscription' => $entity->id()]);
  }

}
