<?php

namespace Drupal\contacts_subscriptions\Form;

use Drupal\contacts_subscriptions\Entity\SubscriptionInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Provides a confirmation form for cancelling a subscription.
 */
class SubscriptionCancelForm extends ConfirmFormBase {

  use SubscriptionFormTrait;

  /**
   * The date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = new static(
      $container->get('date.formatter'),
    );
    $form->csrf = $container->get('csrf_token');
    $form->entityTypeManager = $container->get('entity_type.manager');
    $form->logger = $container->get('logger.channel.contacts_subscriptions');
    return $form;
  }

  /**
   * SubscriptionCancelForm constructor.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   */
  public function __construct(DateFormatterInterface $date_formatter) {
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL, SubscriptionInterface $subscription = NULL) {
    if (!$subscription) {
      throw new AccessDeniedHttpException("Subscription is required for cancellation form.");
    }

    if (!$subscription->isActive()) {
      throw new AccessDeniedHttpException("Only active subscriptions can be cancelled.");
    }

    if (!$subscription->canChangeProduct()) {
      throw new AccessDeniedHttpException("Membership can't be cancelled after being changed.");
    }

    $this->user = $user;
    $this->subscription = $subscription;

    $form = parent::buildForm($form, $form_state);
    $form['actions']['submit']['#attributes']['class'][] = 'btn-danger';

    $form['#cache'] = [
      'contexts' => ['user', 'url.query_args'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contacts_subscription_cancel';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to cancel your membership?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('Your membership is due to renew on @renewal_date. If you cancel, your membership will end on @expiry_date.', [
      '@renewal_date' => $this->dateFormatter->format($this->subscription->getRenewalDate()->getTimestamp(), 'short_date'),
      '@expiry_date' => $this->dateFormatter->format($this->subscription->getExpiryDate()->getTimestamp(), 'short_date'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Cancel membership');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return $this->t('Do not cancel');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url(
      'contacts_subscriptions.manage',
      ['user' => $this->user->id()],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->subscription->getStatusItem()->applyTransitionById('cancel_renewal');
    $this->subscription->save();

    $this->messenger()->addStatus($this->t(
      'Your membership has been set to not renew and will expire on @date.', [
        '@date' => $this->dateFormatter->format($this->subscription->getExpiryDate()->getTimestamp(), 'short_date'),
      ],
    ));

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
