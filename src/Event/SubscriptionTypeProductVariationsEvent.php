<?php

namespace Drupal\contacts_subscriptions\Event;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\contacts_subscriptions\Entity\SubscriptionType;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\user\UserInterface;

/**
 * Event class for determining subscription type specific product variations.
 */
class SubscriptionTypeProductVariationsEvent extends SubscriptionProductVariationsEvent {

  /**
   * The subscription type.
   *
   * @var \Drupal\contacts_subscriptions\Entity\SubscriptionType|null
   */
  protected ?SubscriptionType $subscriptionType;

  /**
   * SubscriptionProductVariationEvent constructor.
   *
   * @param \Drupal\user\UserInterface|null $user
   *   The user entity, if any.
   * @param \Drupal\Core\Entity\ContentEntityStorageInterface $storage
   *   The product entity storage handler.
   * @param \Drupal\contacts_subscriptions\Entity\SubscriptionType $subscription_type
   *   The subscription type to get variants for.
   * @param string|null $variation_suffix
   *   The variation suffix, if any, that we want. By default this will be a
   *   preference. To require a variation suffix, use `::setVariationStrength`.
   */
  public function __construct(
    ?UserInterface $user,
    ContentEntityStorageInterface $storage,
    SubscriptionType $subscription_type,
    ?string $variation_suffix = NULL
  ) {
    parent::__construct($user, $storage, $variation_suffix);
    $this->subscriptionType = $subscription_type;
  }

  /**
   * Get the subscription type for this event.
   *
   * @return \Drupal\contacts_subscriptions\Entity\SubscriptionType|null
   *   The subscription type.
   */
  public function getSubscriptionType() {
    return $this->subscriptionType;
  }

  /**
   * Set the subscription type for this event.
   *
   * @param \Drupal\contacts_subscriptions\Entity\SubscriptionType $subscriptionType
   *   The subscription type for this event.
   */
  public function setSubscriptionType(SubscriptionType $subscriptionType): void {
    $this->subscriptionType = $subscriptionType;
  }

  /**
   * Check whether a variation is suitable.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $variation
   *   The variation we are checking.
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface|null $variation_with_suffix
   *   Variable by reference to store the first variation that matches the
   *   specified suffix.
   *
   * @return bool
   *   Whether the variation should be included in the possible matches.
   */
  protected function filterVariation(ProductVariationInterface $variation, ?ProductVariationInterface &$variation_with_suffix): bool {
    $return = parent::filterVariation($variation, $variation_with_suffix);
    // If also false, no need to check type.
    if (!$return) {
      return FALSE;
    }
    if ($variation->getProduct()->subscription_type->target_id !== $this->subscriptionType->id()) {
      return FALSE;
    }

    return TRUE;
  }

}
