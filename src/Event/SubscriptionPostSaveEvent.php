<?php

namespace Drupal\contacts_subscriptions\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\contacts_subscriptions\Entity\SubscriptionInterface;

/**
 * Event class for Subscription Post Save.
 */
class SubscriptionPostSaveEvent extends Event {

  /**
   * The event name.
   */
  const NAME = 'contacts_subscription_post_save';

  /**
   * The  current subscription.
   *
   * @var \Drupal\contacts_subscriptions\Entity\SubscriptionInterface
   */
  protected SubscriptionInterface $subscription;

  /**
   * Whether the post save occurs after an update.
   *
   * @var bool
   */
  protected bool $update;

  /**
   * Constructs the post save event.
   *
   * @param \Drupal\contacts_subscriptions\Entity\SubscriptionInterface $subscription
   *   The current subscription.
   * @param bool $update
   *   Whether the post save occurs after an update.
   */
  public function __construct(SubscriptionInterface $subscription, bool $update = TRUE) {
    $this->subscription = $subscription;
    $this->update = $update;
  }

  /**
   * Gets the subscription.
   *
   * @return \Drupal\contacts_subscriptions\Entity\SubscriptionInterface
   *   The subscription.
   */
  public function getSubscription(): SubscriptionInterface {
    return $this->subscription;
  }

  /**
   * Gets the update value.
   *
   * @return bool
   *   Whether the save occurred after an update.
   */
  public function getUpdate(): bool {
    return $this->update;
  }

}
