<?php

namespace Drupal\contacts_subscriptions;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\contacts_subscriptions\Entity\SubscriptionInterface;
use Drupal\contacts_subscriptions\Entity\SubscriptionType;
use Drupal\contacts_subscriptions\Event\SubscriptionProductVariationsEvent;
use Drupal\contacts_subscriptions\Event\SubscriptionTypeProductVariationsEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Helper service for Contacts Subscriptions.
 */
class SubscriptionsHelper {

  /**
   * An Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * An event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Constructs a SubscriptionsHelper object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Get a subscription from an order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to get a subscription from.
   *
   * @return \Drupal\contacts_subscriptions\Entity\SubscriptionInterface|null
   *   The subscription from the order or NULL if none found.
   */
  public function getSubscriptionFromOrder(OrderInterface $order): ?SubscriptionInterface {
    return $order->get('subscription')->entity;
  }

  /**
   * Get the applicable product variations.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user we're looking for.
   * @param string|null $suffix
   *   The variation SKU suffix.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface[]
   *   The product variations, keyed by product ID.
   */
  public function getProducts(UserInterface $user, string $suffix = NULL): array {
    $event = new SubscriptionProductVariationsEvent(
      $user,
      $this->entityTypeManager->getStorage('commerce_product'),
      $suffix
    );
    $this->eventDispatcher->dispatch($event::NAME, $event);
    return $event->getProductVariations();
  }

  /**
   * Get the applicable product variations.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user we're looking for.
   * @param \Drupal\contacts_subscriptions\Entity\SubscriptionType $type
   *   The subscription type.
   * @param string|null $suffix
   *   The variation SKU suffix.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface[]
   *   The product variations, keyed by product ID.
   */
  public function getProductsOfType(UserInterface $user, SubscriptionType $type, string $suffix = NULL): array {
    $event = new SubscriptionTypeProductVariationsEvent(
      $user,
      $this->entityTypeManager->getStorage('commerce_product'),
      $type,
      $suffix
    );
    $this->eventDispatcher->dispatch($event::NAME, $event);
    return $event->getProductVariations();
  }

  /**
   * Get the product variation to use.
   *
   * @param \Drupal\contacts_subscriptions\Entity\SubscriptionInterface $subscription
   *   The subscription.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface
   *   The variation to use for the invoice.
   *
   * @throws \InvalidArgumentException
   *   Thrown if we are unable to get the product variation.
   */
  public function getProductVariation(SubscriptionInterface $subscription): ProductVariationInterface {
    $product_id = $subscription->getProductId(FALSE, TRUE);
    if (!$product_id) {
      throw new \InvalidArgumentException("User has no product selected.");
    }

    $product_variations = $this->getProducts($subscription->getOwner());
    if (!$product_variations[$product_id]) {
      throw new \InvalidArgumentException("Unable to find allowed variation for {$product_id}.");
    }

    return $product_variations[$product_id];
  }

}
