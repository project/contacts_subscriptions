<?php

/**
 * @file
 * Hooks provided by the User module.
 */

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\contacts_subscriptions\Entity\SubscriptionInterface;
use Drupal\Core\Url;

/**
 * Alter the redirect when the SubscriptionPaymentForm submits.
 *
 * The hook provides the current subscription, order, url that the form will
 * redirect to. Also contains a success flag, which is TRUE if the payment has
 * succeeded. The $url variable will be where the user gets redirected to on
 * form submit.
 *
 * @param \Drupal\contacts_subscriptions\Entity\SubscriptionInterface $subscription
 *   The current subscription.
 * @param \Drupal\commerce_order\Entity\OrderInterface $order
 *   The current order.
 * @param bool $success
 *   Whether payment has been successfully taken.
 * @param \Drupal\Core\Url $url
 *   The current URL the form will redirect to.
 */
function hook_contacts_subscriptions_alter_payment_redirect(SubscriptionInterface $subscription, OrderInterface $order, bool $success, Url $url) {

  // Redirect the user to their account page only on success.
  if ($success) {
    $url = Url::fromRoute(
      'user.page',
      ['user' => $subscription->getOwnerId()],
    );
  }
}
